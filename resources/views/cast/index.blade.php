@extends('template-adminLTE/master')

@section('tittle')
  Halaman Casts
@endsection

@section('judul')
   <h2>Halaman List Casts</h2>
@endsection
@section('content')

<a href="/cast/create" class="btn btn-success mb-3">Tambah Data Cast</a>

  @if(session('success'))
  <div class="alert alert-info alert-dismissible fade show" role="alert">
    {{session('success')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
  </div>
  @elseif(session('update'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    {{session('update')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
  </div>
  @elseif(session('delete'))
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{session('delete')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
  </div>
  @else
   
  @endif


<table class="table table-bordered">
  <thead class="thead-primary">                  
    <tr>
      <th style="width: 10px" class="text-center">#</th>
      <th style="width: 250px"class="text-center">Nama CAST</th>
      <th style="width: 15px" class="text-center">Umur</th>
      <th class="text-center">Biodata Singkat</th>
      <th style="width: 70px" class="text-center">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse($cast as $key => $item)
      <tr>
        <td class="text-center">{{$key+1}}</td>
        <td>{{$item->nama}} </td>
        <td class="text-center">{{$item->tahun}}</td>
        <td >{{$item->bio}}</td>
        <td class="d-flex text-center">
          <a href="/cast/{{$item->id}}" class="btn btn-info mr-2">Detail</a>
          <a href="/cast/{{$item->id}}/edit" class="btn btn-primary mr-2">Edit</a>
          <form action="/cast/{{$item->id}}" method="POST">
              @csrf
              @method('DELETE')
              <input type="submit" class="btn btn-danger" value="Delete">
          </form>
        </td>
      </tr>

    @empty
      <tr>
        <td class="text-center" colspan="5"><h2>No Cast</h2></td>
      </tr>

    @endforelse
  </tbody>
</table>
@endsection