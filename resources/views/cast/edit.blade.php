@extends('template-adminLTE/master')

@section('tittle')
  Edit Casts
@endsection

@section('judul')
   <h2>Edit Casts</h2>
@endsection
@section('content')
    <form role="form" action="/cast/{{$cast->id}}" method="POST">
    @method('put')
    @csrf
        <div class="form-group">
          <label for="exampleInputPassword1">Nama Pemain Film</label>
          <input type="text" class="form-control" id="exampleInputPassword1" 
          placeholder="Masukkan Nama Pemain Film" name="nama" value="{{$cast->nama}}">
          
        </div>
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
       
        <div class="form-group">
          <label for="exampleInputPassword1">Umur Pemain Film</label>
          <input type="text" class="form-control" id="exampleInputPassword1" 
          placeholder="Masukkan Umur" name="tahun" value="{{$cast->tahun}}">
        </div>
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
       
        
        <div class="form-group">
            <label for="my-textarea">Biodata Singkat Pemain Film</label>
            <textarea id="my-textarea" class="form-control" name="bio" rows="3">{{$cast->bio}}</textarea>
          </div>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror

      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>




@endsection