@extends('template-adminLTE/master')

@section('tittle')
  Halaman Casts{{$cast->nama}}
@endsection

@section('judul')
   <h2>Halaman List Casts {{$cast->nama}}</h2>
@endsection
@section('content')

<a href="/cast" class="btn btn-success mb-3">Kembali List Cast</a>
<table class="table table-bordered">
  <thead class="thead-primary">                  
    <tr>
      <th style="width: 10px" class="text-center">#</th>
      <th style="width: 250px"class="text-center">Nama CAST</th>
      <th style="width: 15px" class="text-center">Umur</th>
      <th class="text-center">Biodata Singkat</th>
    </tr>
  </thead>
  <tbody>
    
      <tr>
        <td>{{$cast->id}}</td>
        <td>{{$cast->nama}}</td>
        <td class="text-center">{{$cast->tahun}}</td>
        <td>{{$cast->bio}}</td>
      </tr>
  </tbody>
</table>
@endsection