<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CastController extends Controller
{
    //
    public function create() {
        
        return view('cast.create');
    }

    public function store(Request $request){
        
        $request->validate([
            'nama' => 'required',
            'tahun' => 'required',
            'bio' =>  'required',
        ],
        [
            'nama.required' => 'Inputan nama harus diisi!',
            'tahun.required' => 'Inputan tahun harus diisi!',
            'bio.required' => 'Inputan biodata harus diisi!',
        ]);


        $query = DB::table('cast')->insert([
            'nama' => $request['nama'],
            'tahun' => $request['tahun'],
            'bio' => $request['bio']
        ]);
        return redirect("/cast")->with('success','Data CAST Berhasil Disimpan');
    }


    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('cast.show',compact('cast')); 
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('cast.edit',compact('cast')); 
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tahun' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'tahun' => $request["tahun"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast')->with('update','Data CAST Berhasil Diupdate');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('delete','Data CAST Berhasil Dihapus');
    }



}
